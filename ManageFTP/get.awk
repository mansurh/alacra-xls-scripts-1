BEGIN {
FS="\t"
OFS=" "
print "user " USER
print PASS
print "binary"
if (DIR != "")
  print "cd " DIR
}

{
str=$1
sub("\r$", "", $1)
print "get \"" $1 "\""
}

END {
print "quit"
}