from lib.DatabaseWorker import DatabaseWorker
from lib.FTP_Doctor import FTP_Doctor
from lib.API_Wizard import API_Wizard
from lib.File_Handler import File_Handler
from lib.SFTP_Finder import SFTP_Finder
import argparse
import logging
import sys
import os
import time

""" Main function for DatabaseWorker.py & FTP_Doctor.py"""
def main():
	#initialize logging file name
	logging.basicConfig(filename='logger.log', filemode='w', level=logging.DEBUG, 
	format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
	
	#command line parsing
	def msg(name='ipid_load'):
		return "python " + name + ".py TODO msg()"
		
	parser = argparse.ArgumentParser(prog='ipid_load', usage=msg())
	subparsers = parser.add_subparsers(dest='command')
	
	c_parser = subparsers.add_parser('create', usage=msg('create'), help='Creates a table using the given arguments.')
	c_parser.add_argument('db_name', type=str)
	c_parser.add_argument('client', type=str)
	c_parser.add_argument('args', type=str, nargs='?')
	c_parser.add_argument('vals', type=str, nargs='?')
	
	u_parser = subparsers.add_parser('update', usage=msg('create'), help='Creates a table using the given arguments.')
	u_parser.add_argument('db_name', type=str)
	u_parser.add_argument('client', type=str)
	u_parser.add_argument('args', type=str, nargs='?')
	u_parser.add_argument('vals', type=str, nargs='?')
	
	c_args = parser.parse_args()
	
	#start database ONLY work 
	database = c_args.db_name #error check for invalid db_name	
	choice = c_args.command #error checking for invalid
	source = c_args.client #error checking for invalid
	logging.info('Client name set to: "' + source + '"')
	databaseWorker=DatabaseWorker(database, source)

	
	logging.info('Retrieving credentials for ' + database + '.')
	creds = databaseWorker.get_creds('Data Servers')
	logging.info('Establishing a connection to ' + database + '.')
	db_conn = databaseWorker.open_connection(creds)
	logging.info('Connection established on ' + database+ '.')

	#end database ONLY work
	isSFTP = databaseWorker.find_ipid_client_data(db_conn, 'isSFTP')
	threshold = databaseWorker.find_ipid_client_data(db_conn, 'threshold')
	delimiter = databaseWorker.find_ipid_client_data(db_conn, 'delimiter')
	regDir = databaseWorker.find_ipid_client_data(db_conn, 'regDir')
	ftpDir = databaseWorker.find_ipid_client_data(db_conn, 'ftpDir')
	#file handler
	file_handler = None
	file_name = None
	unzip = False
	#########

	if c_args.args == None: # this means it is an ftp
		if isSFTP != 'y': #ftp
			ftp_databaseWorker = DatabaseWorker(regDir, source) 
			logging.info('Retrieving credentials for ' + regDir + '.')
			ftp_creds = ftp_databaseWorker.get_creds('File Servers')
			index = ftpDir.rfind('/')
			if index == -1:
				index = 0
			file_name = ftpDir[index:].strip('/')
			if '$date' in file_name:
				date_index = file_name.find('$date')
				date_str = file_name[file_name.find('"')+1:file_name.rfind('"')]
				file_name = file_name[:date_index] + time.strftime(date_str) + file_name[file_name.rfind('"')+1:]
			path = ftpDir[:index]
			ftp_doctor = FTP_Doctor(regDir, path, file_name, db_conn)
			logging.info('Opening FTP connection.')
			ftp_conn = ftp_doctor.open_connection(ftp_creds['dba_server'], ftp_creds['dba_user'], ftp_creds['dba_pass'])
			logging.info('FTP connection is open, retrieving data...')
			ftp_doctor.retrieve_data(ftp_conn, choice)
			if ftp_doctor.is_zip == True:
				unzip = True
				ftp_doctor.unzip_file(ftp_doctor.suffix, db_conn)
			logging.info('Data retrieved, closing FTP connection...')
			ftp_conn.close()
			logging.info('FTP: ' + regDir + ' - connection has been closed.')
		else: #sftp
			sftp_databaseWorker = DatabaseWorker(regDir, source) 
			sftp_creds = sftp_databaseWorker.get_sftp_creds('File Servers')
			index = ftpDir.rfind('/')
			if index == -1:
				index = 0
			file_name = ftpDir[index:].strip('/')
			if '$date' in file_name:
				date_index = file_name.find('$date')
				date_str = file_name[file_name.find('"')+1:file_name.rfind('"')]
				file_name = file_name[:date_index] + time.strftime(date_str) + file_name[file_name.rfind('"')+1:]
			path = ftpDir[:index]
			sftp_finder = SFTP_Finder(regDir, path, file_name, db_conn)
			#sftp = sftp_finder.open_connection_retrieve_data('ftp.alacra.com','matthewtest',None,'C:/cygwin/home/the/.ssh/id_rsa')
			print(sftp_creds)
			sftp = sftp_finder.open_connection_retrieve_data(sftp_creds['dba_server'],sftp_creds['dba_user'],sftp_creds['dba_pass'],sftp_creds['dba_key'])
			time.sleep(3) # without a sleep timer here the file is not fetched fast enough and the program errors out 
			if sftp_finder.is_zip == True:
				unzip = True
				sftp_finder.unzip_file(sftp_finder.suffix, db_conn)
	
	#start database AND ftp work
		#logging.info('File size has been checked, loading data into the table...')
		#delimiter = databaseWorker.find_ipid_client_data(db_conn, 'delimiter')
		#file_handler.load_data(database, creds, delimiter)
		
	#end database AND ftp work
	else:
		a_args = c_args.args.split("|")
		print(a_args)
		a_vals = c_args.vals.split("|")
		print(a_vals)
		argToVal = {}
		for arg in a_args:
			argToVal[arg] = a_vals[a_args.index(arg)]
		try:
			file_name = argToVal['filename']
			if file_name.split('.',1)[0][-4:] == '_new':
				suffix = file_name.split('.',1)[1]
				file_name = file_name.split('.',1)[0][:-4] + '.' + suffix
			if '$date' in file_name:
				date_index = file_name.find('$date')
				file_name = file_name[:date_index] + time.strftime(file_name[date_index+5:].replace('"',''))
		except KeyError:
			logging.error('KeyError: $filename was not given as an argument.')
			print('KeyError: $filename was not given as an argument. Exiting system...')
			sys.exit()	
		extract_data = databaseWorker.find_ipid_client_data(db_conn, 'extractData')
		api_wizard = API_Wizard(source, extract_data, argToVal)
		api_wizard._extract_data() 
	
	file_to_table = databaseWorker.get_table_details(db_conn)
	
	def handle_table(file_name):
		table_name = file_to_table[file_name]
		databaseWorker.set_table_name(table_name)
		if choice == 'create':
			logging.info('Creating a table named ' + table_name + '.')
			databaseWorker.create_table(db_conn, table_name, '')
			logging.info('Creation of ' + table_name + ' complete.')
		elif choice == 'update':
			logging.info('Updating a table named ' + table_name + '.')
			logging.info('Creating a table named ' + table_name + '_new.')
			databaseWorker.create_table(db_conn, table_name, '_new')
			logging.info('Creation of ' + table_name + '_new complete.')
		elif choice == 'increment':
			logging.info('Applying an incremental update on ' + table_name + '.')
			print("TODO Incremental Update")
		else:
			logging.warning('Invalid first argument: ' + choice + '... must be "create"|"update"|"increment"')
			print('Invalid first argument: ' + choice + '... must be "create"|"update"|"increment"')
			sys.exit()
	
	
	def handle_file(file_name):
		og_file = file_name.replace('_new','')
		print('og_file ---------' + og_file)
		table_name = databaseWorker.get_table_details(db_conn)[og_file]
		print(table_name)
		databaseWorker.table_name = table_name
		file_handler = File_Handler(source, regDir, table_name, file_name)
		logging.info('Checking file size...')
		file_handler.check_file_size(file_name, threshold)
		logging.info('File size has been checked, loading data into the table...')
		file_handler.load_data(database, creds, delimiter, choice, unzip)
		logging.info('Data has been loaded into ' + table_name)

		if choice == 'update':
			max_deviation = databaseWorker.find_ipid_client_data(db_conn, 'deviationAllowed')
			logging.info('Checking table deviation, max deviation=' + str(max_deviation) + '.')
			file_handler.find_deviation(db_conn, max_deviation)
			logging.info('Deviation has been checked, renaming the tables...')
			databaseWorker.rename_tables(db_conn)
			logging.info('Tables have been renamed.')
			logging.info('Renaming file...')
			file_handler.rename_files(file_name)
			logging.info('Files have been renamed.')
			
	def handle_zip_directory():
		file_list = os.listdir(loaddir + '/zip_files')
		for file in file_list:
			file_name = file
			print(file_list)
			print('file_name: '+ file_name)
			handle_table(file_name)
		if choice == 'update':
			for file in file_list:
				index = file.find('.')
				print('this one')
				unix_command = 'mv ' + loaddir + '/zip_files/' + file + ' ' + loaddir + '/zip_files/' + file[:index] + '_new' + file[index:]
				os.popen(unix_command)
			time.sleep(1)
			file_list = os.listdir(loaddir + '/zip_files')
		print('file_list!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ' + str(file_list))
		for file in file_list:
			#file_handler = File_Handler(source, regDir, databaseWorker.table_name, file)
			unix_command = 'cp ' + loaddir + '/zip_files/' + file + ' ' + loaddir
			os.popen(unix_command).read()
			handle_file(file)
			unix_command = 'rm ' + loaddir + '/' + file
			os.popen(unix_command).read()
	
	loaddir = os.environ.get('XLSDATA') + '/' + regDir 
	if unzip == True:
		#if choice == 'update':
		#	file_list = os.listdir(loaddir + '/zip_files')
		#	for file in file_list:
		#		index = file.find('.')
		#		unix_command = 'mv ' + loaddir + '/zip_files/' + file + ' ' + loaddir + file[:index] + '_new' + file[index:]
		#		os.popen(unix_command)
		#	handle_zip_directory()
		#else:
			#file_list = os.listdir(loaddir + '/zip_files')
			#for file in file_list:
			#	unix_command = 'mv ' + loaddir + '/zip_files/' + file + ' ' + loaddir + '/' + file
			#	os.popen(unix_command)
		handle_zip_directory()

	else:
		handle_table(file_name)
		handle_file(file_name)
	
	file_handler = File_Handler(source, regDir, databaseWorker.table_name, file_name)
	logging.info('Creating load directory...')
	file_handler.create_load_directory()
	logging.info('Load directory created.')
	#closing connections
	db_conn.close()
	logging.info('Database: ' + database + ' - connection has been closed.')
	
	
	#THIS SHOULD ALWAYS BE THE LAST FUNCTION TO RUN!!
	logging.info('Storing log file.')
	file_handler.store_log()
	#start logging email
	unix_command = 'blat logger.log -to matthew.pitcher@alacra.com'
	os.popen(unix_command)
	#THIS SHOULD ALWAYS BE THE LAST FUNCTION TO RUN!!
	

if __name__ == '__main__':
	main()