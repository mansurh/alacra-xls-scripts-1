from db_retriever import DB_Retriever
from file_hand import File_Hand
import argparse
import sys
import os
import time
import traceback
from log_me import logger

def init_args(arguments):
	args = {}
	keys = ['client_dir','table_name','max_deviation','stage','origin_database','destination_database','origin_server','destination_server','create_procedure','load_procedure','parse']
	index = 0
	for key in keys:
		args[key] = arguments[index]
		index += 1
	return args

def main():
	#command line parsing
	def msg(name='ipid_load'):
		return "python " + name + ".py TODO msg()"
		
	parser = argparse.ArgumentParser(prog='ipid_load', usage=msg())
	parser.add_argument('client_dir', type=str)
	
	args = parser.parse_args()
	client_dir = args.client_dir
	
	scriptdir = os.environ.get('XLS') + '/src/scripts/ReferencePython/generic_loading_tables/'
	loaddir = loaddir = os.environ.get('XLSDATA') + '/' + client_dir + '/'
	unix_command = 'mkdir -p ' + loaddir + '/log'
	print(os.popen(unix_command).read())
	today = time.strftime('%Y%m%d')
	unix_command = 'mkdir -p ' + loaddir + '/back/' + today
	print(os.popen(unix_command).read())
	
	stage = 'prod'
	logging = logger(client_dir + '_python_.log',loaddir,loaddir+'log/',stage)
	fh = File_Hand(client_dir,logging)
	
	print(args)
	logging.info(str(args))
	
	conc_worker = DB_Retriever('concordance', None, None, None, logging) # concordance connection
	creds = conc_worker.get_creds("Data Servers")
	#server = args.server
	#if server:
	#	creds['dba_server'] = server
	print(creds)
	conc_conn = conc_worker.open_connection(creds) # origin connection
	logging.info("Established connection with the origin database")
	conc_worker.set_connection(conc_conn)
	sql_command = "select * from generic_load where client_dir = '" + client_dir + "'"
	arguments = conc_worker.get_data(sql_command).fetchone()
	args2 = init_args(arguments)
	print(args2)
	djw_worker = DB_Retriever(args2['origin_database'], None, sql_command, client_dir, logging) # origin database worker
	creds = djw_worker.get_creds("Data Servers")
	if args2['origin_server']:
		creds['dba_server'] = args2['origin_server']
	djw_conn = djw_worker.open_connection(creds) # origin connection
	logging.info("Established connection with the origin database")
	djw_worker.set_connection(djw_conn)
	
	con_worker = DB_Retriever(args2['destination_database'], None, sql_command, client_dir, logging) # destination database worker
	creds = con_worker.get_creds("Data Servers") # credentials for the destination db
	if args2['destination_server']:
		creds['dba_server'] = args2['destination_server']
	con_conn = con_worker.open_connection(creds) # destination connection
	logging.info("Established connection with the destination database")
	con_worker.set_connection(con_conn)
	#print creds / sys.exit()
	data = djw_worker.get_data(args2['load_procedure']).fetchall()
	print(data[0][0])
	logging.info("Data from the load file has been fetched: " + str(len(data)))
	if args2['parse'] != None: # parse or no parse?
		#parsed = fh.parser(param[0],param[1],param[2].split(','), data)
		#data = fh.update_data_with_parsed(param[0],data,parsed)
		print('PARSE TODO - PARTIALLY COMPLETE')
		#logging.info("Data has been parsed using the parse file.")
	
	table_name = 'ipid_' + args2['client_dir']
	if args2['table_name'] != None: table_name = args2['table_name']
	file_name = table_name + '.txt'
	fh.put_data_in_file(file_name, data)
	logging.info("BCP upload file has been created.")
	
	con_worker.execute_command(args2['create_procedure'])Numb
	
	print(args2['destination_database'])
	unix_command = 'bcp ' + args2['destination_database'] + '..' + table_name +  '_new in ' + loaddir + file_name + ' -U' + creds['dba_user'] + ' -P' + creds['dba_pass'] + ' -S' + creds['dba_server'] + ' -c -t"|" -CRAW -e ' + loaddir + table_name + '.err'
	print(os.popen(unix_command).read())
	logging.info("Data has been uploaded to the table via BCP.")
	#for sql_command in destination_commands:
	#	if sql_command[1][0:6] == 'select':
	#		data = con_worker.get_data(sql_command[1]).fetchall()
	#	else:
	#		con_worker.execute_command(sql_command[1])
	'''
	###TODO DESTINATION SQL COMMANDS POST TABLE TODO###
	'''
	
	max_deviation = args2['max_deviation']
	if con_worker.find_deviation(con_conn, max_deviation):
		con_worker.rename_tables2(table_name)
		logging.info("Deviation test has passed, tables have been renamed.")
	else:
		logging.info("Deviation test stopped the system.")
		
	logging.info("Load directories have been created.")
	if fh.check_file_size(file_name, 50):
		fh.move_files(file_name)
		#for key in files:
			#fh.move_arg_files(client_dir, files[key])
		logging.info("Files have been moved to their load directory.\n\n")
	else:
		logging.info('File size check has stopped the system.')
	
	logging.end_success()
	

if __name__ == '__main__':
	main()
	
	