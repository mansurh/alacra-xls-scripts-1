origin|IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ipid_djrc_python_table_data]') AND type in (N'U'))      
DROP TABLE [dbo].[ipid_djrc_python_table_data]   
<><>
origin|create table ipid_djrc_python_table_data (    
[id] int,    
[entityName] varchar(500), 
[suffix] varchar(100),   
[addressLine] varchar(300),
[city] varchar(100),    
[country] varchar(100),    
[RN] varchar(200),    
[content_type] varchar(100)      
)
<><>
origin|WITH ipid_djrc_python_data(id,entityName,suffix,addressLine,city,country,RN,content_type) AS
(
select distinct  n.id, 
master.dbo.anglicize(replace(n.entityName, '|', ' ')), 
master.dbo.anglicize(replace(n.suffix, '|', ' ')), 
master.dbo.anglicize(replace(c1.addressLine, '|', ' ')) as 'addressLine', 
master.dbo.anglicize(replace(c1.city, '|', ' ')),c2.name  as 'country', 
ROW_NUMBER() OVER(Partition By n.id ORDER BY country, addressLine ) as 'RN','DJW' as ContentType 
from  NameDetailsDJW  n  
left join CountryDetailsDJW c on c.id = n.id 
left join CompanyDetailsDJW c1 on    convert(varchar(30),c.id) + c.countrycode = convert(varchar(30),c1.entityId) + c1.country 
left join CountryListDJW  c2 on c.countrycode = c2.code 
where   n.nameType = 'Primary Name' and n.idType=2 and n.id not in (select id from NameDetailsSOC) 
and c.countryType = 'Country of Affiliation' 
and c.countrycode in ( select top 1 countrycode from  CountryDetailsDJW  
where countryType = 'Country of Affiliation' and id=n.id order by countrycode asc) 
union 
select distinct n.id, 
master.dbo.anglicize(replace(n.entityName, '|', ' ')), 
master.dbo.anglicize(replace(n.suffix, '|', ' ')), 
master.dbo.anglicize(replace(c1.addressLine, '|', ' ')) as 'addressLine', 
master.dbo.anglicize(replace(c1.city, '|', ' ')),  
c2.value as 'country', 
ROW_NUMBER() OVER(Partition By n.id ORDER BY country, addressLine ) as 'RN','SOC' as ContentType 
from NameDetailsSOC n  
left join CompanyDetailsSOC c1 on   n.id=c1.entityId 
left join CountryListSOC  c2 on c1.country = c2.CountryId  
where nameType = '1' and n.idType=2 and n.id not in (select id from NameDetailsDJW) 
union 
select distinct  n.id, 
master.dbo.anglicize(replace(n.entityName, '|', ' ')), 
master.dbo.anglicize(replace(n.suffix, '|', ' ')), 
master.dbo.anglicize(replace(c1.addressLine, '|', ' ')) as 'addressLine', 
master.dbo.anglicize(replace(c1.city, '|', ' ')), 
c2.name  as 'country', 
ROW_NUMBER() OVER(Partition By n.id ORDER BY country, addressLine ) as 'RN','DJW/SOC' as ContentType 
from  NameDetailsDJW  n  
left join CountryDetailsDJW c on c.id = n.id 
left join CompanyDetailsDJW c1 on    convert(varchar(30),c.id) + c.countrycode = convert(varchar(30),c1.entityId) + c1.country 
left join CountryListDJW  c2 on c.countrycode = c2.code 
where   n.nameType = 'Primary Name' and n.idType=2 and n.id in (select id from NameDetailsSOC) 
and c.countryType = 'Country of Affiliation' 
and c.countrycode in ( select top 1 countrycode from  CountryDetailsDJW where countryType = 'Country of Affiliation' and id=n.id order by countrycode asc) 
) 
insert  into ipid_djrc_python_table_data 
select id, entityName, suffix, addressLine,city,country,RN,content_type  from ipid_djrc_python_data where RN=1 
<><>
origin|update ipid_djrc_python_table_data set entityName = entityName + ' ' + suffix where suffix is not null and suffix != '' 
<><>
origin|update ipid_djrc_python_table_data 
       set country = name from CountryListDJW c 
       join CompanyDetailsDJW c1 on c1.country = c.code
       where id = c1.entityId
       and ipid_djrc_python_table_data.country = 'Not Known'
<><>
origin|select id,entityName,addressLine,city,country,content_type from ipid_djrc_python_table_data 
<><>
destination|update ipid_djrc_new set state = REPLACE(state,'Â','')
<><>
destination|update ipid_djrc_new set state = sp.name from state_prov sp where state in (select sp.abbreviation)
<><>
destination|update ipid_djrc_new 
				set name = ida.name 
				from ipid_djrc_alias ida 
				where ipid_djrc_new.id = ida.id  
				and ida.type in ('Also Known As', 'Formerly Known As') 
				and ida.name not like '%?%' 
				and ipid_djrc_new.name like '%?%' 