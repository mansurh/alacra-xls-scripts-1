# Script file to generate the IP matching reports for Nelson Earnings Estimates
# Created by Colin duSaire February 12, 2002
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_nelson.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=nelson

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in Nelson Earnings database */
PRINT "Potentially new companies in Nelson Earnings Estimates"
select * from ipid_nelson
where matchkey not in (select sourcekey from company_map where source=11)


/* Required Report - Companies which have disappeared from Nelson Earnings database */
PRINT "Matched companies that have disappeared from Nelson Earnings Estimates"
select "AlacraID"=x.id, "Name"=x.name, "Country"=c.name
from company x, company_map m, country c
where
x.id = m.xlsid and x.country*=c.id and m.source=11
and
m.sourcekey not in (select matchkey from ipid_nelson)

/* Optional report - new information available in database */
PRINT "Nelson URLs where Alacra has no url"
select "AlacraID"=a.id, "Alacra Name"=a.name, "A_Country"=b.name, 
"A_Url"=a.url, "Nelson Name"=c.name, "N_Country"=c.country, "N_Url"=RTRIM(c.url)
from 
	company a, country b, ipid_nelson c, company_map m
where 
	a.country*=b.id and RTRIM(a.url)='' 
	and a.id=m.xlsid and m.source=11 
	and m.sourcekey=c.matchkey and RTRIM(c.url)!=''


HERE
