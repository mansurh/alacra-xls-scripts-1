# Arguments:
# 1 = ipid Server
# 2 = ipid User
# 3 = ipid Password
# 3 = ipid dbname
if [ $# -ne 4 ]
then
	echo "Usage: $0.sh server user password ipname"
	exit 1
fi

IPIDserver=$1
IPIDlogin=$2
IPIDpassword=$3
ipname=$4

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b /Q "exec log_mappings_date '${ipname}'" 
if [ $? -ne 0 ]
then
	echo "error updating updatedate in mappings table for ${ipname}, exiting ..."
	exit 1
fi

exit 0