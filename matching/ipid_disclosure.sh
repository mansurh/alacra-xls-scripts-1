# Script file to build the IP matching table for Investext
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
if [ $# -lt 3 ]
then
	echo "Usage: ipid_disclosure.sh xls_server xls_login xls_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3

ipidname=ipid_disclosure
SCRIPTDIR=$XLS/src/scripts/matching
TMPDIR=f:/dga/${ipidname}

#create temporary directory
mkdir -p ${TMPDIR}

# Name of the temp file to use
logfile=${TMPDIR}/${ipidname}_log.txt

# Run ipid update.
echo ""
echo "${ipidname}.sh: IPID update for Disclosure" > ${logfile}

# Perform the update
${SCRIPTDIR}/${ipidname}_go.sh ${xlsserver} ${xlslogin} ${xlspassword} >> ${logfile} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} -ne 0 ]
then
	echo "Bad return code from ${ipidname}_go.sh - mailing log file" >> ${logfile}
	blat ${logfile} -t "simon.vileshin@alacra.com" -s "IPID update for Disclosure Failed."
else
	# good return code, mark update complete in DBA database
	#blat ${logfile} -t "simon.vileshin@alacra.com,companymatch@alacra.com" -s "IPID update for Disclosure finished successfully."
	rm -f ${logfile}
fi


exit $returncode
