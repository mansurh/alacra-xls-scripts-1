IF EXISTS(SELECT * FROM sys.indexes WHERE name='ipid_dnb01' AND object_id = OBJECT_ID('ipid_dnb_new'))        
BEGIN 
	DROP INDEX [ipid_dnb01] on ipid_dnb_new 
END

IF EXISTS(SELECT * FROM sys.indexes WHERE name='ipid_dnb02' AND object_id = OBJECT_ID('ipid_dnb_new'))        
BEGIN 
	DROP INDEX [ipid_dnb02] on ipid_dnb_new 
END


CREATE NONCLUSTERED INDEX [ipid_dnb01] ON [dbo].[ipid_dnb_new] 
(
	[dunsNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


CREATE CLUSTERED INDEX [ipid_dnb02] ON [dbo].[ipid_dnb_new] 
(
	[strDunsNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
