IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
				 WHERE
                 TABLE_NAME = 'ipid_bvdorbis' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
	drop table ipid_bvdorbis
END

CREATE TABLE [dbo].[ipid_bvdorbis](
       [BvD_ID] [varchar](20) NULL,
       [Company_name] [nvarchar](510) NULL,
       [street_line1] [nvarchar](2048) NULL,
       [Postal_code] [varchar](40) NULL,
       [City] [varchar](100) NULL,
       [Country_iso_code] [varchar](16) NULL,
       [Ticker] [varchar](32) NULL,
       [ISIN] [varchar](12) NULL,
       [SEDOL] [varchar](12) NULL,
       [status] [varchar](32) NULL,
       [location_desc] [varchar](32) NULL,
       [Main_exchange] [varchar](64) NULL,
       [template] [varchar](16) NULL,
       [legal_form]	[varchar](128) NULL,
       [turnover_USD] [varchar](16) null
) ON [PRIMARY]