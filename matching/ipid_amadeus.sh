# Script file to build the IP matching table for Amadeus M&A
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = amadeus Server
# 6 = amadeus Login
# 7 = amadeus Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_amadeus.sh IPID_server IPID_database IPID_login IPID_password amadeus_server amadeus_login amadeus_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
amadeusserver=$5
amadeuslogin=$6
amadeuspassword=$7

ipname=amadeus

# Name of the temp file to use
TMPFILE1=ipid_amadeus1.tmp
TMPFILE2=ipid_amadeus2.tmp

# Name of the table to use
TABLENAME=ipid_amadeus

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the bmi data into a temporary file
# Get the most recent version of the company name for the matching
# process
isql -S${amadeusserver} -U${amadeuslogin} -P${amadeuspassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON

select BVDNumber,ConsolidationCode,VOBVDNumber,CompanyName,Address,ZipCode,City,Phone,Fax,Country,ListedIndicator,ISIN,
NACE,turnover,employees,ISOCurrency,ISOCountryCode
from company
order by companyname

HERE

# Step 3 - post-process the temp file
sed -f match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (

        BVDNumber varchar(20) not NULL,
        ConsolidationCode varchar(2) NULL,
        VOBVDNumber varchar(20) NULL, 
        CompanyName varchar(200) NULL, 
        Address varchar(200) NULL,
        ZipCode varchar(200) NULL,
        City varchar(200) NULL,
        Phone varchar(200) NULL,
        Fax varchar(200) NULL,
        Country varchar(200) NULL,
        ListedIndicator int NULL,
        ISIN varchar(20) NULL,
        NACE varchar(200) NULL,
        turnover float NULL,
        employees float NULL,
        ISOCurrency varchar(200) NULL,
        ISOCountryCode varchar(200) NULL
)


GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(BVDNumber)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(CompanyName)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c /t "|" /b1000000 /e ${TABLENAME}.err
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi


