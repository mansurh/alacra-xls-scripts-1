# Script file to generate the IP matching reports for Factiva
# Output deletes file restricts by Type 3 Security
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_factiva.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=factiva

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in factiva"
select * from ipid_factiva
where FDSCode not in (select sourcekey from company_map where source=9943)


/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from factiva"
select x.id, x.name, "Country"=c.name, "Exchange"=y.name
from company x, company_map m, country c, security s, exchange y
where
x.id = m.xlsid and m.source=9943
and
m.sourcekey not in (select FDSCode from ipid_factiva)
and 
x.country=c.id
and
x.id=s.issuer and s.type = 3 and s.exchange*=y.id

/* Optional report - new information available in database */

HERE
