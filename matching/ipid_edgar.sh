XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script file to build the IP matching table for Edgar
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_edgar.sh ipid_server ipid_database ipid_login ipid_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
FTPserver=`get_xls_registry_value edgar Server 6`
FTPlogin=`get_xls_registry_value edgar User 6`
FTPpassword=`get_xls_registry_value edgar Password 6`

# Name of the temp file to use
TMPFILE1=TFS_BCP.txt
TMPFILE2=edgar.pip

# Name of the table to use
TABLENAME=ipid_edgar

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_edgar.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - get the edgar data into a temporary file
getFilename='TFS_BCP.txt' 

echo "user ${FTPlogin}" > zipfile.ftp
echo "${FTPpassword}" >> zipfile.ftp
echo "binary" >> zipfile.ftp
echo "get $getFilename" >> zipfile.ftp
echo "quit" >> zipfile.ftp

ftp -i -n -s:zipfile.ftp ${FTPserver}
if [ $? != 0 ]
then
	echo "ftp errored out"
	exit 1
fi 


# Step 3 - post-process the temp file
sed -e "s/~/|/g" < ${getFilename} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	ticker varchar(8) NOT NULL,
	name varchar(64) NOT NULL,
	type varchar(64) null,
	CIK varchar(10) NOT NULL,
	market varchar(32) null,
	flag char(2)null,
	id char(8) null,
    market_name varchar(64) null,
    market_desc varchar(64) null
	
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(CIK)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
