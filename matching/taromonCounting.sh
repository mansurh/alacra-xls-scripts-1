#This script runs the taromonCounts webservice 
#and if the test is bad, throw an error.
#Requires mks sh and web commands
#Call this in your shell script like this:
#(Take out the first column #)
# #begin code
# ./taromonCounting.sh <name_in_eli..Counting_table>
# if [ $? -eq 0 ]
# then
#	#echo "success"
# else
# 	echo "taromonCounting detected an error!"
# 	exit 1
# fi
# #end code
#
# where <name_in_eli..Counting_table> is a test name in the 
# eli..Counting table


test=$1
#echo "testCounting.sh $1"

res=`web get http://taromon.alacra.com/taromon/taromonCounting.pl?testid=$test`
if [ $res == "pass" ]
then
	#print "$res"
	return 0
else
	return 1
fi

