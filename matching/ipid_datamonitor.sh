# Script file to build the IP matching table for Datamonitor
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = datamonitor Server
# 6 = datamonitor Login
# 7 = datamonitor Password
ARGS=7
if [ $# -ne $ARGS ]
then
    echo "Usage: ipid_datamonitor.sh ipid_server ipid_database ipid_login ipid_password dm_server dm_login dm_password"
    exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogon=$3
IPIDpassword=$4
srcdataserver=$5
srclogon=$6
srcpassword=$7


LOADDIR=${XLS}/src/scripts/loading/datamonitor

#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${srcdataserver}"
echo ""

#isql -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -r -Q "set nocount on select distinct doc_id + '|' + rtrim(replace(prop_value, 'Company Profile', '')) from datamonitor.dbo.doc_props where prop_name='title'" > ${TEMPDIR}/ipid_dm.dat.temp1
bcp "select distinct doc_id + '|' + rtrim(replace(prop_value, 'Company Profile', '')) from datamonitor.dbo.doc_props where prop_name='title'" queryout ${TEMPDIR}/ipid_dm.dat.temp1 -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} /c /t"|"

cat ${TEMPDIR}/ipid_dm.dat.temp1 | sed -f ${LOADDIR}/ipid_datamonitor.sed > ${TEMPDIR}/ipid_dm.dat.temp2

#while read -r line
#do

#    line=${line%% }

#    if [ "${line}" != '' ]
#    then

#        print ${line}

#    fi

#done < ${TEMPDIR}/ipid_dm.dat.temp2 > ${TEMPDIR}/ipid_dm.dat
cp ${TEMPDIR}/ipid_dm.dat.temp2 ${TEMPDIR}/ipid_dm.dat
#
# Drop old data from xls table ipid_datamonitor
#

echo ""
echo "Dropping old ipid_datamonitor data on ${IPIDserver}"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/create_ipid_datamonitor.sql


# drop the indices for the table
echo ""
echo "Dropping indices for ipid_datamonitor for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/dropindex_ipid_datamonitor.sql


#
# Load all the new data...
#

DATFILE="${TEMPDIR}/ipid_dm.dat"

# Load the ipid_datamonitor table
echo "Loading the ipid_datamonitor table" 

bcp ${IPIDdatabase}.dbo.ipid_datamonitor in ${DATFILE} /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} /f${LOADDIR}/ipid_datamonitor.fmt /e${TEMPDIR}/ipid_datamonitor.log
if [ $? != 0 ]
then

    echo "Error loading ipid_datamonitor table"
    exit 1

fi

# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_datamonitor for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/createindex_ipid_datamonitor.sql

echo ""
echo "Updated table ipid_datamonitor for Datamonitor OK."
echo ""

exit 0
