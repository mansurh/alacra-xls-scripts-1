# Script file to generate the IP matching reports for mergent
#
# History: 
#	05/01/01 - Created - MZ
#	Removed emailing of report as this is already handled
#	by xrefdb. Colin duSaire 02/12/02
#
# 1 = Output file name
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password

if [ $# -lt 4 ]
then
	echo "Usage: ipid_mergent_report.sh outputfile, xls server, xls login, xls password"
	exit 1
fi

# Save the runstring parameters in local variables 

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

cd ${XLS}/src/scripts/matching

if [ -s ${REPORTFILE} ] 
then
	rm ${REPORTFILE}
fi

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE

SET NOCOUNT ON

/* New companies */
PRINT "Potentially new companies in mergent"
select i.entityreference, i.legalname from ipid_mergent i
where i.entityreference not in (select sourcekey from company_map where source = 39)

/* Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from mergent"
select x.id, x.name, "Country"=c.name, m.sourcekey, "Exchange"=y.name
from company x, company_map m, country c, exchange y, security s
where x.id = m.xlsid and m.source = 39 
and x.country=c.id and x.id=s.issuer and s.type=3 and s.exchange*=y.id
and m.sourcekey not in (select entityreference from ipid_mergent)

HERE

