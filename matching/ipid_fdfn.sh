# Script file to build the IP matching table for fdfn
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = fdfn Server
# 5 = fdfn Login
# 6 = fdfn Password
if [ $# -lt 6 ]
then
    echo "Usage: ipid_fdfn.sh xls_server xls_login xls_password fdfn_server fdfn_login fdfn_password"
    exit 1
fi

destdataserver=$1
destlogon=$2
destpassword=$3
srcdataserver=$4
srclogon=$5
srcpassword=$6


LOADDIR=${XLS}/src/scripts/loading/fdfn
TEMPDIR=${XLSDATA}/matching


#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${srcdataserver}"
echo ""

isql -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -r -Q " set nocount on select distinct rtrim(ltrim(d1.prop_value)) + '|' + rtrim(ltrim(d1.prop_value)) + '|' + convert(varchar(8), d2.prop_value) from fdfn.dbo.doc_props d1, fdfn.dbo.doc_props d2 where d1.prop_name='comp' and d2.prop_name='tick' and d1.doc_id=d2.doc_id order by 1 " -w 255 -x 255 > ${TEMPDIR}/ipid_fdfn.dat.temp1

tail +5 < ${TEMPDIR}/ipid_fdfn.dat.temp1 | sed -f ${LOADDIR}/ipid_fdfn.sed > ${TEMPDIR}/ipid_fdfn.dat.temp2

while read -r line
do

    line=${line%% }

    if [ "${line}" != '' ]
    then

        print ${line}

    fi

done < ${TEMPDIR}/ipid_fdfn.dat.temp2 > ${TEMPDIR}/ipid_fdfn.dat

#
# Drop old data from xls table ipid_fdfn
#

echo ""
echo "Dropping old ipid_fdfn data on ${destdataserver}"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/create_ipid_fdfn.sql


# drop the indices for the table
echo ""
echo "Dropping indices for ipid_fdfn for faster loading"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/dropindex_ipid_fdfn.sql


#
# Load all the new data...
#

DATFILE="${TEMPDIR}/ipid_fdfn.dat"

# Load the ipid_fdfn table
echo "Loading the ipid_fdfn table" 

bcp xls.dbo.ipid_fdfn in ${DATFILE} /U${destlogon} /P${destpassword} /S${destdataserver} /f${LOADDIR}/ipid_fdfn.fmt /e${TEMPDIR}/ipid_fdfn.log -m 2000
if [ $? != 0 ]
then

    echo "Error loading ipid_fdfn table"
    exit 1

fi

# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_fdfn for faster loading"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/createindex_ipid_fdfn.sql

echo ""
echo "Updated table ipid_fdfn for fdfn OK."
echo ""

exit 0
