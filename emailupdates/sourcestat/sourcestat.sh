XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_return_code.fn
. $XLSUTILS/get_registry_value.fn

if [ $# -gt 0 ]
then
	dbserver=$1
else
	dbserver=`get_xls_registry_value concordance Server`
fi

dbuser=`get_xls_registry_value concordance user`
dbpass=`get_xls_registry_value concordance password`

updatedate=`date +%Y%m%d`
dataload=$XLSDATA/sourcestat
logdir=$XLSDATA/sourcestat/log
mkdir -p ${logdir}
logfile=${logdir}/sourcestat_${updatedate}.log

mkdir -p $dataload
cd $dataload
check_return_code $? "Error cd to ${dataload}" 1

echo "Executing query" > ${logfile}
mysql="select '<tr><td style=\"   color:#ff6800; font-weight:bold;\">', 'Total Records Inbound'as '<td>Source</td>',': ', count(Unique_ID) as '<td>Count</td>','</td></tr>' from ipid_wachovia_rq
union
select '<tr><td style=\"  color:#1c2e3f; font-weight:bold;\">', 'Mappings by Vendors:' as '<td>Source</td>', '</td><td><div style=\"display:none; visibility:hidden\">', ' ' as 'Count', '</div></td></tr>'
union
select '<tr><td style=\" border: 1px solid black;\">' as ' ',ms.dbNameLong as '<td>Source</td>','</td><td style=\"border: 1px solid black;\">' as ' ', count(distinct i.legal_id) as '<td>Count</td>','</td></tr>' as '</tr>'
from company_map m, ipid_wachovia_cn2 i, mappings ms where m.source=111111 and m.sourcekey=i.legal_id
and i.vendor_code_value=ms.id and i.legal_id in (select Unique_id from ipid_wachovia_rq)
group by i.vendor_code_value, ms.dbNameLong --order by name
union
select '<tr><td style=\"color:#ff6800; font-weight:bold;\">', 'Total Wells with 1+ mapping' as '<td>Source</td>',': ', count(distinct Legal_ID) as 'Count','</td></tr>' from ipid_wachovia_cn2 where Vendor_Code_Value<>111111"

echo "Emailing query results" > ${logfile}
bcp "${mysql}" queryout sourcestat_${updatedate}.txt /S${dbserver} /U${dbuser} /P${dbpass} /c
check_return_code $? "Error in bcp out sourcestat" 1
(echo "<h1 style="color:#1c2e3f">Alacra Weekly Statistics:</h1><table style=\"border-collapse:collapse\" cellpadding=\"4\">"; cat sourcestat_${updatedate}.txt) >tmpfile.txt
cp tmpfile.txt sourcestat_${updatedate}.html
rm tmpfile.txt
echo "</table>" >> sourcestat_${updatedate}.html
 
#send email to people
blat sourcestat_${updatedate}.html -s "Wells Weekly Statistics ${updatedate}" -t wcisstats@opus.com

if [ $? != 0 ]
then
  	#send email to admin
  	blat ${logfile} -s "Sourcestat ${updatedate} send failed" -t administrators@alacra.com
  	exit 1
fi
