TITLEBAR="mergstat emonthly report update"

updatedate=`date +%m%d%y`
logfilename=elog${updatedate}.log

# Perform the update
$XLS/src/scripts/emailupdates/mergstat/emailupdate.sh ddl1.alacra.com emonthly ylhtnome >> ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "No email found from ddl1, check ddl2 now"
	$XLS/src/scripts/emailupdates/mergstat/emailupdate.sh ddl2.alacra.com emonthly ylhtnome >> ${logfilename} 2>&1
	returncode=$?
	if [ ${returncode} != 0 ]
	then
		echo "No email found from emailupdate.sh - mailing log file" >> ${logfilename}
		blat ${logfilename} -t "tun.he@alacra.com" -s "Warning: No email from FactSet emonthly report today"
		exit 1
	fi
fi

exit 0
