# emailupdate.sh
#	1 = POP3 server
#	2 = email user name
#	3 = email password
#	4 = name for the message (for inserted db data)
#
shname=emailupdate.sh
if [ $# -lt 3 ]
then
	echo "Usage: ${shname} emailserver emailuser emailpsw msgname"
	exit 1
fi

# Save the runstring parameters in local variables
emailserver=${1}
emailuser=${2}
emailpsw=${3}
msgname=${4}
if [ $# -gt 4 ]
then
	acctcode=${5}
else
	acctcode="NULL"
fi

#echo "emailserver=${emailserver}"
#echo "emailuser=${emailuser}"
#echo "emailpsw=${emailpsw}"
#echo "msgname=${msgname}"

thismonth=`date +%m`
thisyear=`date +20%y`
month=""

if [ "${thismonth}" = "01" ]
then
	month=January
elif [ "${thismonth}" = "02" ]
then
	month=February
elif [ "${thismonth}" = "03" ]
then
	month=March
elif [ "${thismonth}" = "04" ]
then
	month=April
elif [ "${thismonth}" = "05" ]
then
	month=May
elif [ "${thismonth}" = "06" ]
then
	month=June
elif [ "${thismonth}" = "07" ]
then
	month=July
elif [ "${thismonth}" = "08" ]
then
	month=August
elif [ "${thismonth}" = "09" ]
then
	month=September
elif [ "${thismonth}" = "10" ]
then
	month=October
elif [ "${thismonth}" = "11" ]
then
	month=November
elif [ "${thismonth}" = "12" ]
then
	month=December
fi

if [ $# -gt 3 ]
then
	month=${4}
fi

thisfile=${month}${thisyear}.pdf

# wipe out any existing mail files
alldeletes="MSG*.TXT* extracted.pdf"
for onedel in $alldeletes
do
	rm -f ${onedel}
	if [ $? != 0 ]
	then
		echo "${shname}: could not remove ${onedel}, exiting"
		exit 1
	fi
done

# retrieve mail via getmail
echo "${shname}: Retrieving Mail"
getmail.exe -S ${emailserver} -U ${emailuser} -P ${emailpsw}
mailret=$?
if [ mailret -gt 1 ]
then
	echo "${shname}: Error in getmail, Exiting"
	echo ""
	exit 1
elif [ mailret -eq 1 ]
then
	echo "${shname}: getmail reports no mail today, exiting"
	echo ""
	exit 1
fi


# any mail files that emerged will be named MSGnnnn.TXT
#for mailfilename in `ls MSG*.TXT`
#do
	echo "${shname}: Processing ${mailfilename}"

	thislink="http://www.factset.com/mergerstat_em/monthly/US_Flashwire_Monthly.pdf"

	echo "Use wget to retrieve file from ${thislink}"
	wget "${thislink}" -O ${thisfile}

	echo "ftp file to ftp.alacra.com"
	rm -f command.ftp
	echo "user mstat" > command.ftp
	echo "ryan0705" >> command.ftp
	echo "binary" >> command.ftp
	echo "put ${thisfile}" >> command.ftp
	echo "quit" >>command.ftp
	ftp -i -n -s:command.ftp ftp.alacra.com
	if [ $? != 0 ]
	then
		echo "Error in ftp, exiting"
	exit 1
	fi

	echo ""
	echo ""
#done

exit 0
