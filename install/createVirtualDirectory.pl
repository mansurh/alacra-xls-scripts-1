
use strict;

use Win32::OLE;
use Win32::OLE 'in';

my $vdirname = $ARGV[0];
my $servername = "Default Website";
if( $#ARGV ge 1 ) {
	$servername = $ARGV[1];
}

# ADSI
my $website = undef;
my $svc = Win32::OLE->GetObject('IIS://LocalHost/W3SVC');
foreach my $srvr (in $svc) {
	if ($srvr->{ServerComment} eq "Default Website") {
		$website = $srvr;
	}
}
my $serverroot = $website->GetObject("IIsWebVirtualDir", "ROOT");
my $dir = $serverroot->GetObject("IIsWebVirtualDir", $vdirname);
if( !defined($dir) ) {
	$dir = $serverroot->Create("IIsWebVirtualDir", $vdirname);
	$dir->Put("Path", "C:\\usr\\netscape\\server\\docs" . $vdirname);
	$dir->Put("AppFriendlyName", $vdirname . "App");
	$dir->Put("AppIsolated", 2);  # 0 = low, 1 = high, 2 = medium
	$dir->Put("AccessScript", "True");
	$dir->Put("AccessWrite", "True");
	$dir->Put("AccessRead", "True");
	$dir->Put("AccessExecute", "True");
	$dir->AppCreate(1);
	$dir->SetInfo();
} else {
	print "$vdirname already exists!\n";
}

1;

