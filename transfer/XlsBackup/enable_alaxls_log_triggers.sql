ENABLE TRIGGER prepbooktoc_del
ON prepbooktoc        
go      
ENABLE TRIGGER prepbooktoc_insupd      
ON prepbooktoc      
go
ENABLE TRIGGER prepbook_del      
ON prepbook        
go      
ENABLE TRIGGER prepbook_insupd      
ON prepbook      
go
ENABLE TRIGGER pbusage_insupd      
ON pbusage      
go
ENABLE TRIGGER pbusage_nodelete          
ON pbusage            
go
ENABLE TRIGGER usage_insupd    
ON usage    
go 
ENABLE TRIGGER usage_nodelete    
ON usage      
go
ENABLE TRIGGER shoppingcart_del    
ON shoppingcart      
go
ENABLE TRIGGER shoppingcart_insupd    
ON shoppingcart    
go
ENABLE TRIGGER shoppingcartlog_nodelupd    
ON shoppingcartlog      
