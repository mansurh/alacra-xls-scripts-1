DISABLE TRIGGER prepbooktoc_del
ON prepbooktoc        
go      
DISABLE TRIGGER prepbooktoc_insupd      
ON prepbooktoc      
go
DISABLE TRIGGER prepbook_del      
ON prepbook        
go      
DISABLE TRIGGER prepbook_insupd      
ON prepbook      
go
DISABLE TRIGGER pbusage_insupd      
ON pbusage      
go
DISABLE TRIGGER pbusage_nodelete          
ON pbusage            
go
DISABLE TRIGGER usage_insupd    
ON usage    
go 
DISABLE TRIGGER usage_nodelete    
ON usage      
go
DISABLE TRIGGER shoppingcart_del    
ON shoppingcart      
go
DISABLE TRIGGER shoppingcart_insupd    
ON shoppingcart    
go
DISABLE TRIGGER shoppingcartlog_nodelupd    
ON shoppingcartlog      
