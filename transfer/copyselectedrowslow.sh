XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/compare_bcp_row_counts.fn

#parse the command line arguments
# 1 = source query
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest table
# 7 = dest db
# 8 = dest server
# 9 = dest user
# 10 = dest passwd
# 11 = temp file

shname=$0
if [ $# -lt 11 ]
then
    echo "Usage: ${shname} srcquery srcdb srcserver srclogin srcpasswd desttable destdb destserver destlogin destpasswd tempfile [truncflag]"
    exit 1
fi


query=${1}
srcdb=${2}
srcserver=${3}
srcuser=${4}
srcpasswd=${5}
desttable=${6}
destdb=${7}
destserver=${8}
destuser=${9}
destpasswd=${10}
tempfile=${11}
if [ $# -lt 12 ]
then
	truncflag=0
else
	truncflag=${12}
fi


# delete temp files
rm -f ${tempfile}*

tempfilein=${tempfile}.in
tempfileout=${tempfile}.out



echo "${shname}: Transfer of selected rows from ${srcdb} (${srcserver}) to ${desttable} table in ${destdb} (${destserver})"

# dump the data out
$XLS/src/scripts/transfer/dumpselectedrows.sh "${query}" ${srcdb} ${srcserver} ${srcuser} ${srcpasswd} ${tempfile}
if [ $? != 0 ]; then echo "${shname}: Error in dumpselectedrows.sh"; exit 1; fi

# truncate, if asked
if [ "${truncflag}" = "1" ]
then
	echo "Truncating ${destserver}.${destdb}.${desttable} table"
	isql /b /U${destuser} /P${destpasswd} /S${destserver} /d ${destdb} /Q"truncate table ${desttable}"
	if [ $? != 0 ]; then echo "Error truncating  ${desttable} in ${destdb}"; exit 1; fi;
fi

# load the data in
$XLS/src/scripts/transfer/loaddataonly.sh ${desttable} ${destdb} ${destserver} ${destuser} ${destpasswd} ${tempfile} ${truncflag}
if [ $? != 0 ]; then echo "${shname}: Error in loaddataonly.sh"; exit 1; fi

compare_bcp_row_counts ${tempfileout} ${tempfilein} ${destserver} ${destdb} ${destuser} ${destpasswd} ${desttable} ${truncflag}

exit 0
