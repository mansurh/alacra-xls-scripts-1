#parse the command line arguments
# 1 = sourcedb
# 2 = source server
# 3 = source login
# 4 = source passwd
# 5 = dest db
# 6 = dest server
# 7 = dest user
# 8 = dest passwd
# 9 = sites universe

if [ \( "$#" -lt 8 \) -o \( "$#" -gt 9 \) ]
then
    echo "Usage: transferSiteTable.sh srcdb srcserver srclogin srcpasswd destdb destserver destlogin destpasswd [siteuniverse]"
    exit 1
fi

srcdb=$1
srcserver=$2
srcuser=$3
srcpasswd=$4
destdb=$5
destserver=$6
destuser=$7
destpasswd=$8

TEMPDIR=$XLSDATA
rm -f ${TEMPDIR}/transfer.dat

echo "\nTransfer of sites table from ${srcdb} (${srcserver}) to sites table in ${destdb} (${destserver})"

if [ "$#" -eq 8 ]
then
	bcp "exec COPY_SITES_TABLE" queryout ${TEMPDIR}/transfer.dat /S${srcserver} /U${srcuser} /P${srcpasswd} /c /t"\001" /r"\002"
else
	bcp "exec COPY_SITES_TABLE '$9'" queryout ${TEMPDIR}/transfer.dat /S${srcserver} /U${srcuser} /P${srcpasswd} /c /t"\001" /r"\002"
fi

if [ $? != 0 ]
then
    echo "Error saving source sites table data to file"
    exit 1
fi

isql /n /U${destuser} /P${destpasswd} /S${destserver} /Q"use ${destdb} truncate table sites"

if [ $? != 0 ]
then
    echo "Error truncating sites table in ${destdb}"
    exit 1
fi

echo "\nLoading data into destination sites table in ${destdb}..."

bcp ${destdb}.dbo.sites in ${TEMPDIR}/transfer.dat /b1000 /S${destserver} /U${destuser} /P${destpasswd} /c /t"\001" /r"\002"

if [ $? != 0 ]
then
    echo "Error copying data to destination sites table in ${destdb}"
    exit 1
fi
