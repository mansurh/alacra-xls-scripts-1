XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/compare_bcp_row_counts.fn

#parse the command line arguments
# 1 = table name
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest table
# 7 = dest db
# 8 = dest server
# 9 = dest user
# 10 = dest passwd
# 11 = temp file

shname=$0
if [ $# -lt 11 ]
then
    echo "Usage: ${shname} srctable srcdb srcserver srclogin srcpasswd desttable destdb destserver destlogin destpasswd tempfile"
    exit 1
fi

srctable=${1}
srcdb=${2}
srcserver=${3}
srcuser=${4}
srcpasswd=${5}
desttable=${6}
destdb=${7}
destserver=${8}
destuser=${9}
destpasswd=${10}
tempfile=${11}

rm -f ${tempfile}*

tempfilein=${tempfile}.in
tempfileout=${tempfile}.out

echo "${shname}: Transfer of ${srctable} table from ${srcdb} (${srcserver}) to ${desttable} table in ${destdb} (${destserver})"

$XLS/src/scripts/transfer/dumpdataonly.sh ${srctable} ${srcdb} ${srcserver} ${srcuser} ${srcpasswd} ${tempfile}
if [ $? != 0 ]; then echo "${shname}: Error in dumpdataonly.sh"; exit 1; fi

$XLS/src/scripts/transfer/loaddataonly.sh ${desttable} ${destdb} ${destserver} ${destuser} ${destpasswd} ${tempfile}
if [ $? != 0 ]; then echo "${shname}: Error in loaddataonly.sh"; exit 1; fi

compare_bcp_row_counts ${tempfileout} ${tempfilein} ${destserver} ${destdb} ${destuser} ${destpasswd} ${desttable} 1

exit 0
