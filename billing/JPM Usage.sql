DECLARE @startdate datetime
DECLARE @enddate datetime

select @startdate='October 1, 2002'
select @enddate='November 1, 2002'

select 'JPM Usage'

select 
	i.name 'Database', RTRIM(b.pbname) 'Alacra Book', RTRIM(b.pbcompanyname) 'Company', p.login,u.project,u.project2,u.project3,u.access_time,u.description,u.list_price 
from 
	usage u JOIN users p ON u.userid=p.id JOIN shoppingcart s ON
	u.shoppingcartid = s.shoppingcartid LEFT JOIN prepbook b ON
	s.prepbookid = b.prepbookid JOIN ip i ON u.ip = i.id
where 
	u.userid in (select id from users where account in (2542,2984) and demo_flag is NULL)
	and 
	access_time >= @startdate 
	and 
	access_time < @enddate
	and 
	no_charge_flag is null
	and 
	u.ip not in (1,125)


union


select 
	i.name 'Database', NULL 'Alacra Book', NULL 'Company', p.login,u.project,u.project2,u.project3,u.access_time,u.description,u.list_price 
from 
	usage u, users p, ip i
where 
	u.userid in (select id from users where account in (2542,2984) and demo_flag is NULL)
	and 
	access_time >= @startdate 
	and 
	access_time < @enddate
	and 
	no_charge_flag is null
	and 
	u.userid=p.id
	and 
	((u.shoppingcartid is null) or (u.shoppingcartid not in (select shoppingcartid from shoppingcart)))
	and
	u.ip = i.id
	and
	u.ip not in (1,125)


order by i.name, p.login, u.access_time asc
