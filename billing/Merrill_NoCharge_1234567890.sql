update usage set no_charge_flag=1 where usageid in (
select u.usageid from usage u, users p where 
u.userid = p.id
and
(u.project='1234567890' or u.project='1234567890_ALACRA')
and 
p.company like 'Merrill%'
and
u.access_time >= 'January 1, 2009'
and 
u.no_charge_flag is NULL
)
