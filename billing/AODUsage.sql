select 
	i.id 'Database Id', 
	i.name 'Database Name', 
	a.id 'Account Id', 
	a.name 'Account Name', 
	p.login 'Login',
	u.project 'Project',
	u.project2 'Project 2',
	u.project3 'Project 3',
	u.project4 'Project 4',
	u.project5 'Project 5',
	u.access_time 'Access Time',
	u.description 'Content Description',
	u.list_price 'List Price',
	u.price 'Alacra Billed Price'
from usage u, users p, account a, ip i
where
u.userid = p.id
and p.account = a.id
and u.ip = i.id
and p.service=79
and p.demo_flag is NULL
and p.terminate >= u.access_time
and u.access_time >= 'April 1, 2015' and u.access_time < 'May 1, 2015'