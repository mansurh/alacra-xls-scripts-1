set transaction isolation level read uncommitted

select 
	i.id 'Database Id', 
	i.name 'Database Name', 

	case when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') then 5745
	else a.id
	end 'Account Id',

	case when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') then 'Merrill Lynch India'
	else a.name
	end 'Account Name',

	p.login 'Login',
	u.project 'Project',
	u.project2 'Project 2',
	u.project3 'Project 3',
	u.project4 'Project 4',
	u.project5 'Project 5',
	u.access_time 'Access Time',
	u.description 'Content Description',
	u.list_price 'List Price',
	u.price 'Alacra Billed Price',
	s.ipinfo 'Opt 1|Opt 2|Opt 3|Opt 4'
from 
	usage u JOIN users p ON u.userid=p.id LEFT JOIN usagespecial s ON
	u.usageid = s.usage JOIN ip i ON u.ip = i.id JOIN account a ON
	p.account = a.id
where 
	ip in 	(
		select 
			id 
		from 
			ip 
		where 
			ipcode in (
				'TECH',
				'IBES',
				'EXTEL',
				'MARKI',
				'CARSON',
				'EXECCO',
				'IINSID',
				'SDCJV',
				'SDCVC',
				'DGA',
				'FIRSTC',
				'FCRSH',
				'FCNOTE',
				'DSTRM',
				'FDFN',
				'WORLDS',
				'EXCARD',
				'WSAPI',
				'INSID',
				'CCBN',
				'TFMMN',
				'ITAPI',
				'TFSHRE'
				)
		)
	and 
	no_charge_flag is null
	and 
	access_time >= 'September 1, 2007' and access_time < 'October 1, 2007'
	and
	p.demo_flag is NULL
	and

	(
		/* Boston Consulting Group */
		userid in (select id from users where account in (select id from account where name like '%bcg%' or name like '%Boston Consulting%'))
		or

		/* Charles River */
		userid in (select id from users where account in (4125,1406))
		or

		/* Greenhill */
		userid in (select id from users where account in (2065,3548,3798))
		or 

		/* KPMG */
		u.userid in (select id from users where account in (select id from account where name like '%kpmg%'))
		or

		/* JP Morgan */
		u.userid in (select id from users where account in (2542,2984))
		or

		/* McKinsey */
		userid in (select id from users where account in (select id from account where name like '%McKinsey%'))
		or 

		/* Morgan Stanley */
		userid in (select id from users where account = 2594)
		or

		/* Merrill Lynch */
		userid in (select id from users where account in (select id from account where name like '%Merrill Lynch%'))	
		or

		/* UBS US */
		userid in (select id from users where account in (2934,3210))
		or

		/* UBS UK */
		userid in (select id from users where account in (3014,3184))
		or

		/* PriceWaterhouseCoopers */
		userid in (select id from users where a.name like 'Pricewaterhouse%' or a.id in (2649,3474))	
		or

		/* TD Securities */
		userid in (select id from users where account in (select id from account where name like '%TD Securities%'))		
	)
	
order by 
	i.id, a.id, p.login, u.access_time asc
