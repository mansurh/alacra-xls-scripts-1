# Split the command line arguments.
#	1 = server
#	2 = user
#	3 = password
set arglist [split $argv]
if {[llength $arglist] != 3} {
	puts "Usage: dbsize.tcl server user password"
	exit
}

# Open connections to SQL Server
set handle [sybconnect [lindex $arglist 1] [lindex $arglist 2] [lindex $arglist 0]]

# List of databases
set dblist {}


# Get the list of databases
sybsql $handle "select name,dbid from master..sysdatabases"
sybnext $handle {
	lappend dblist { @1 @2 }
}

# Output the column headings
set outputString [format "%-20s %12s %12s" "" "SIZE" "FREE"]
puts "$outputString"
set outputString [format "%-20s %12s %12s" "" "====" "===="]
puts "$outputString"

# Now process each database
foreach db $dblist {

	# set name [string trim @1]
	# set id [string trim @2]

	set name [lindex $db 0]
	set id [lindex $db 1]

	sybsql $handle "declare @dbsize dec(15,0) dbcc updateusage($name) with no_infomsgs select @dbsize = sum(convert(dec(15),size)) from master..sysusages where dbid = $id select  database_name = '$name', database_size = @dbsize / 512, 'unallocated space' = (@dbsize - (select sum(convert(dec(15),reserved)) from $name..sysindexes where indid in (0, 1, 255)))/512"

	# Skip first two result sets
	sybnext $handle { }
	sybnext $handle { }

	sybnext $handle { 
		if { (@3 / @2) < 0.10 } {
			set outputString [format "%-20s %12.1f %12.1f  ***** LOW FREE SPACE *****" @1 @2 @3]
		} else {
			set outputString [format "%-20s %12.1f %12.1f" @1 @2 @3]
		}
		puts "$outputString"
	}
} 
# Close the connections to SQL Server 
sybclose $handle
