# Setting buffer and output file names
BUFFER='/users/xls/src/scripts/reports/database/temp.buffer'
STATSLIST='/users/xls/src/scripts/reports/database/temp.stats_list'
LOWFILE='/users/xls/src/scripts/reports/database/temp.low'

# Clean up from last run
rm -f ${STATSLIST}

# The server containing the dba database
DBASERVER=`$XLS/src/scripts/utility/dbserver.sh dba`
DBAUSER=dba
DBAPASSWORD=dba

# get names of server that stat reporting is to be carried out on.
isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n << EOF > $BUFFER

use dba
go
set nocount on
go
select name from dbserver
go

EOF

server_list="$(cat $BUFFER | sed '1,2d;s/^ //;s/^$//;/^$/d')"
echo "$server_list"
for server in $server_list
do

isql /Usa /Pnewpass /S$server -n << EOF > $BUFFER
use master
go
set nocount on
go
select name from master..sysdatabases
go
EOF

db_list="$(cat $BUFFER | sed '1,2d;s/^ //;s/^$//;/^$/d')"
echo "$db_list"
for db in $db_list
do

echo "doing db $db"

# isql /Usa /Pnewpass /S$server -n << EOF
# use master
# go
# dbcc updateusage($db)
# go
# EOF

isql /Usa /Pnewpass /S$server -n << EOF | tr -s ' ' > $BUFFER
use master
go
set nocount on
go
declare @dbsize dec(15,0)
select @dbsize = sum(convert(dec(15),size))
from master..sysusages
where dbid = db_id("$db")

select ltrim(rtrim(@@servername)),
ltrim(rtrim("$db")),
(@dbsize/512),
(@dbsize-(select sum(convert(dec(15), reserved)) 
from $db..sysindexes 
where indid in (0,1,255)))/512
go
EOF

cat $BUFFER | sed '1,2d;s/^ //;/^$/d' >> $STATSLIST

done
done

echo "going through statslist"

cat $STATSLIST | sed '1,2d;s/---*//g;/^$/d'| awk -f awk.sh | while read servername dbname size freespace
do

	echo "$servername, $dbname, $size, $freespace"


	if [ "$servername" = "" ]
	then
		continue
	fi
	if [ "$dbname" = "" ]
	then
		continue
	fi
	if [ "$size" = "" ]
	then
		continue
	fi
	if [ "$freespace" = "" ]
	then
		continue
	fi

isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} << EOF
use dba
go
exec addDBStats "$servername","$dbname",$size,$freespace
go
EOF

done

isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n /w 500 << EOF | sed '7D' > ${LOWFILE}
use dba
go
set nocount on
go
print " "
print "DATABASES REPORTING LOW FREE SPACE"
print "----------------------------------"
print " "
print " "
print " Server   Database   Size       Free       Date "
select convert(varchar(8),s.name),
       convert(varchar(10),d.name),
       convert(varchar(10),t.size),
       convert(varchar(10),t.free),
       measured      
from dbserver s, dbase d, dbstats t, dbinstance i
where t.measured = (select max(t2.measured)
                from dbstats t2
                where t2.dbinstance = t.dbinstance)
and ((t.free/t.size) < 0.10)
and DATEPART(day,t.measured) = DATEPART(day,getdate())
and DATEPART(month,t.measured) = DATEPART(month,getdate())
and DATEPART(year,t.measured) = DATEPART(year,getdate())
and t.dbinstance = i.id
and i.dbase = d.id
and i.dbserver = s.id

EOF

# Mail databases with low free space to administrators
blat ${LOWFILE} -t administrators@xls.com -s "DB LOW SPACE"
