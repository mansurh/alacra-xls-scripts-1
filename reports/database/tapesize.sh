# The server containing the dba database
DBASERVER=`$XLS/src/scripts/utility/dbserver.sh dba`
DBAUSER=dba
DBAPASSWORD=dba

isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n << EOF 

use dba
go

/* Compute size of databases dumped on each tape */
select t.tname, sum(s.size) "Backup Size (megabytes)" from dbtape t, dbtapedatabase b, dbstats s
where
t.tname = b.tname
and
t.dbserver = lower(s.dbserver)
and
b.dbname = s.dbname
and
s.measured = (select max(measured) from dbstats where dbname=b.dbname and lower(dbserver)=t.dbserver)
group by t.tname
go

EOF
