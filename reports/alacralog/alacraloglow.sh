TITLEBAR="Alacralog Extract Report"

# Parse the command line arguments.
#   1 = account number
#   2 = year as YYYY
#   3 = month as MM
#	4 = email To: list
#	5 = userid exclude list (comma separated) - optional
if [ $# -lt 4 ]
then
	echo "Usage: alacraloglow.sh account year month emailtolist"
	exit 1
fi

# Save the runstring parameters in local variables
account=$1
year=$2
month=$3
emailtolist=$4
if [ $# -lt 5 ]
then
	useridexcludelist=""
	userexcludeclause=""
else
	useridexcludelist=$5
	userexcludeclause="-X ${useridexcludelist}"
fi


outputdir1=$XLSDATA/alacralog
# convert all forward slashes to back slashes
# because blat doesn't accept forward slashes on attachment names
outputdir=$(sed -e"s/\//\\\\/g" << HERE
${outputdir1}
HERE
)

# Perform the update
echo "alacralogreport.exe -D ${outputdir} -A ${account} -Y ${year} -M ${month} ${userexcludeclause}"
alacralogreport.exe -D ${outputdir} -A ${account} -Y ${year} -M ${month} ${userexcludeclause}
if [ $? != 0 ]
then
	echo "Bad return code from alacralogreport.exe - failing job"
	exit 1
fi

ssfilename="${outputdir}\\UsageLog${account}_${year}_${month}.xls"
if [ ! -s ${ssfilename} ]
then
	echo "${ssfilename} does not exist or is zero length - failing job"
	exit 1
fi


echo "Attached is a web log report for account ${account} for the month ${month}/${year}" > $XLSDATA/alacralog/temp.tmp

# now e-mail the resulting file
echo "blat $XLSDATA/alacralog/temp.tmp -attach ${ssfilename} -t ""${emailtolist}"" -s ""Web log report for account ${account} for the month ${month}/${year}"""


blat $XLSDATA/alacralog/temp.tmp -attach ${ssfilename} -t "${emailtolist}" -s "Web log report for account ${account} for the month ${month}/${year}"

rm $XLSDATA/alacralog/temp.tmp


exit 0
