XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

if [ $# -ne 1 ]
then
	echo "Usage: UpdateAnalysisCubes.sh as_server"
	exit 1
fi

as_server=$1
domainuser="ALACRA.NET\\prodbuild"
domainpass="!R)Nmountain"

scriptdir=$XLS/src/scripts/reports/analysis


process_cube()
{
	if [ $# -lt 2 ]
	then
		return 1
	fi
	ASDBNAME=$1
	CUBENAME=$2
	FILESUFFIX=$3

	retval=0

	echo "Processing Dimensions (ProcessUpdate) in ${CUBENAME} cube in Analysis Services database ${ASDBNAME} on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d ${ASDBNAME} -i ${scriptdir}/processDim${FILESUFFIX}.xmla -o processDimout${FILESUFFIX}.xml -T processDim${FILESUFFIX}.trace
	ssaserrcheck.exe processDimout${FILESUFFIX}.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Dimensions (processDim${FILESUFFIX}.xmla)"
		retval=1
	fi

	echo "Processing Cube (ProcessFull) ${CUBENAME} in Analysis Services database ${ASDBNAME} on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d ${ASDBNAME} -i ${scriptdir}/processCube${FILESUFFIX}.xmla -o processCubeout${FILESUFFIX}.xml -T processCube${FILESUFFIX}.trace
	ssaserrcheck.exe processCubeout${FILESUFFIX}.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Cube (processCube${FILESUFFIX}.xmla)"
		retval=1
	fi

	return $retval
}



run() {
	retval=0

	process_cube Pulse PulseEventStats ""

	process_cube AlacraStore AlacraStore _AlacraStore

	process_cube DMOmonitor DMOmonitorStats _DMOmonitor

	process_cube AlacraPremium Usage _AlacraPremium

	process_cube AAFStats AAFStats _AAFStats

	return $retval
}


if [ "${XLSDATA}" = "" ]
then
	echo "Warning, XLSDATA not defined, using f:/users/default"
	XLSDATA=f:/users/default
	if [ ! -d "f:/users/default" ]
	then
		mkdir -p f:/users/default
		if [ $? != 0 ]
		then
		    echo "XLSDATA is not set up, exiting"
		    exit 1
		fi
	fi
fi

if [ ! -d $XLSDATA/AnalysisServices/log ]
then
	mkdir -p $XLSDATA/AnalysisServices/log
	if [ $? != 0 ]
	then
	    echo "XLSDATA/AnalysisServices/log is not set up, exiting"
	    exit 1
	fi
fi

cd $XLSDATA/AnalysisServices

logfile=log/UpdateAnalysisCubes`date +%y%m%d`.log
run > ${logfile}
if [ $? -ne 0 ]
then
	blat ${logfile} -t administrators@alacra.com -s "Update of Cubes on Analysis Services Server $as_server failed."
	exit 1
else
	successmsg="Update of Cubes on Analysis Services Server $as_server was successful."
	echo "${successmsg}" > success_email.txt
	echo "Log file at http://${COMPUTERNAME}/xlsdata/AnalysisServices/${logfile}" >> success_email.txt

#	blat success_email.txt -t administrators@alacra.com -s "${successmsg}"
	rm success_email.txt
fi

exit 0
