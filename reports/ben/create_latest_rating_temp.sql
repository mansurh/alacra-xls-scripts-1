use concordance
go

if exists (select * from dbo.sysobjects where id = object_id(N'[latest_rating_temp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [latest_rating_temp]
go

CREATE TABLE latest_rating_temp
(
	local_id	varchar(255) NULL,
	latest_doc_id	varchar(255) NULL,
	latest_doc_date	varchar(255) NULL,
	url	varchar(512) NULL,
)
GO

CREATE INDEX latest_rating_temp_01 ON latest_rating_temp(local_id)
GO
