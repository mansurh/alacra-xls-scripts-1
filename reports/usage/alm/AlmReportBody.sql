set transaction isolation level read uncommitted
set nocount on

DECLARE @month datetime
select @month=CONVERT(datetime,(CONVERT(varchar(7),DATEADD(month,-1,GETDATE()),120)+'-01'))

select 
a.name 'Firm',
p.first_name 'First Name', 
p.last_name 'Last Name', 
(select count(*) from usage u where u.userid = p.id and u.access_time >= DATEADD(month,-5,@month) and u.access_time < DATEADD(month,-4,@month)) as 'Month -5',
(select count(*) from usage u where u.userid = p.id and u.access_time >= DATEADD(month,-4,@month) and u.access_time < DATEADD(month,-3,@month)) as 'Month -4',
(select count(*) from usage u where u.userid = p.id and u.access_time >= DATEADD(month,-3,@month) and u.access_time < DATEADD(month,-2,@month)) as 'Month -3',
(select count(*) from usage u where u.userid = p.id and u.access_time >= DATEADD(month,-2,@month) and u.access_time < DATEADD(month,-1,@month)) as 'Month -2',
(select count(*) from usage u where u.userid = p.id and u.access_time >= DATEADD(month,-1,@month) and u.access_time < @month) as 'Month -1',
(select count(*) from usage u where u.userid = p.id and u.access_time >= @month and u.access_time < DATEADD(month,1,@month)) as 'Current',
(select COALESCE (CONVERT(varchar(30),max(access_time),126),'2000-01-01') from usage u where u.userid = p.id) as 'Last Download',
p.login
from users p, account a 
where 
p.id in (select id from users where login like 'lmi2-%'
)
and p.terminate >= GETDATE()
and p.account = a.id
and a.id in (select account from account_perm where (ip=270 or ip=296) and permission=1)
order by a.name asc, p.last_name asc, p.first_name asc