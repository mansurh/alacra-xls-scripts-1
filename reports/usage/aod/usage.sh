#!/bin/bash

XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

set -o pipefail
set -u

current=$(pwd)

mkdir -p logs
logfiledate=$(date +%Y-%m-%d-t%H%M)
logfile="${current}/logs/${logfiledate}-aod-monthly-stats.log"

exec > >(tee -a "${logfile}")
exec 2> >(tee -a "${logfile}" >&2)

function handle_err {
    cp ${logfile} error.mailing.log
    blat error.mailing.log -t "administrators@alacra.com" -s "AOD monthly stats job error"
    rm -f error.mailing.log
	exit $1
}

date
echo "AOD monthly stats job"
echo

DBSERVER=`get_xls_registry_value xls server`
DBUSER=`get_xls_registry_value xls user`
DBPASSWORD=`get_xls_registry_value xls password`

if [[ -z $DBSERVER || -z $DBUSER || -z $DBPASSWORD ]]; then
    echo "Failed to get xls server, user, or pass from registry";
    handle_err 1;
fi

rm -f nul
rm -f output.csv
rm -f output_results.csv


year=$(date +%Y)
this_month=$(date +%m)
# http://stackoverflow.com/questions/13168463/using-date-command-to-get-previous-current-and-next-month
last_month=$(date --date="$(date +%Y-%m-15) -1 month" +%m)


# if bcp creates the file, the permissions it creates are not good enough for sed
touch output_results.csv
chmod 644 output_results.csv

echo
echo "Executing exec aod_usageStatsInternalMailing '${year}-${last_month}-01', '${year}-${this_month}-01'"
bcp "exec aod_usageStatsInternalMailing '${year}-${last_month}-01', '${year}-${this_month}-01'" queryout output_results.csv \
    -S $DBSERVER -U $DBUSER -P $DBPASSWORD \
    -c -t "@|@|" -CRAW
if [ $? -ne 0 ]; then { "Error in bcp" && handle_err 1; } fi

sed -i 's/,//g' output_results.csv
sed -i 's/@|@|/,/g' output_results.csv
                      
echo "userid,registration_date,email,name,company,state,country,usage_row_count,total_purchased" > output.csv
cat output_results.csv >> output.csv


echo
echo "Mailing stats out"
blat $XLS/src/scripts/reports/usage/aod/body.html \
     -to "matthew.strauss@alacra.com,carolann.thomas@alacra.com,donald.roll@alacra.com" \
     -subject "AOD monthly stats ${last_month}/${year}" \
     -attach output.csv
if [ $? -ne 0 ]; then { echo "Blat error" && handle_err 1; } fi
