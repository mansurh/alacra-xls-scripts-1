BEGIN {
	FS="|"
}
{
	a[NR] = $4
}
END {
	for (i = 1; i<=NR; ++i) {
		printf "%s",a[i]
		if (i < NR)
			printf "|"
		else
			printf "\n"
	}
}

