XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

database="concordance"
emailDestination="cicistats@alacra.com"

# Get the SQL Server to connect to
dbserver=`get_xls_registry_value ${database} Server`
dbuser=`get_xls_registry_value ${database} User`
dbpassword=`get_xls_registry_value ${database} Password`

echo "GEI Stats for ${database} on ${dbserver}"

if [ "${dbserver}" = "datatest2" ]
then
	emailDestination="anthony.bruni@alacra.com"
fi

# Start the report output file
cat< $XLS/src/scripts/reports/GEIStats/preamble.xml >report.xml
if [ $? != 0 ]
then
	echo "Error creating report.xml file, terminating"
	exit 1
fi

for Sheet in General Country State LegalForm hasGEI hasRegisterNo Profile
do
	echo "Current Sheet: ${Sheet}"

	# Run the queries for the individual sheets

	# convert all forward slashes to backslashes
	fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
$XLS/src/scripts/reports/GEIStats/${Sheet}.sql
HERE
)

	sqlcmd -S ${dbserver} -U ${dbuser} -P ${dbpassword} -i ${fwslashpath1} -o ${Sheet}.rpt -W -s"|" -h -1
	if [ $? != 0 ]
	then
		echo "Error running SQLQuery ${Sheet}.sql, terminating"
		exit 1
	fi 

	sed -f $XLS/src/scripts/reports/GEIStats/${Sheet}.sed < ${Sheet}.rpt > ${Sheet}.xml
	if [ $? != 0 ]
	then
		echo "Error creating XML file from ${Sheet}.rpt, terminating"
		exit 1
	fi 

	# Must be in the UTF-8 Character Set
	iconv -f ISO-8859-1 -t UTF-8 < ${Sheet}.xml > ${Sheet}_utf.xml
	if [ $? != 0 ]
	then
		echo "Error converting output to UTF-8, terminating"
		exit 1
	fi 


	# Append together the Excel bits
	for bit in $XLS/src/scripts/reports/GEIStats/${Sheet}Start.xml ${Sheet}_utf.xml $XLS/src/scripts/reports/GEIStats/WorksheetEnd.xml
	do
		cat <${bit}  >> report.xml
		if [ $? != 0 ]
		then
			echo "Error concatenating ${bit} to report file, terminating"
			exit 1
		fi 
	done

	# Clean Up
	rm -f ${Sheet}.rpt
	rm -f ${Sheet}.xml
	rm -f ${Sheet}_utf.xml
done

# End the report output file
cat < $XLS/src/scripts/reports/GEIStats/postamble.xml >>report.xml
if [ $? != 0 ]
then
	echo "Error wrapping up report.xml file, terminating"
	exit 1
fi

# Zip the report file
zip report.zip report.xml
if [ $? != 0 ]
then
	echo "Error zipping report.xml file, terminating"
	exit 1
fi



# Mail it out.  Must be a binary attachment as UTF-8 uses the high bit
blat -to "${emailDestination}" -subject "GEI Statistics" -body "See Attached" -uuencode -attach report.zip
if [ $? != 0 ]
then
	echo "Error sending report file via Blat, terminating"
	exit 1
fi


# Clean up final file
# rm -f report.xml
# rm -f report.zip

exit 0