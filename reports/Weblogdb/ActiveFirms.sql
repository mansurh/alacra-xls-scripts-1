set nocount on
set transaction isolation level read uncommitted

/* Active Firms */
select distinct 'firmID'=b.id, 'firmName'=b.name, t.firm_type
from firms b, analyst_firm c, analyst_firm_type t
where b.id=c.firm_id
and c.analyst_type=t.id
and c.date_ended is null 
order by b.name