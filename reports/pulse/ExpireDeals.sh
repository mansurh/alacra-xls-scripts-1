XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - server
#	2 - emailDest
if [ $# -lt 2 ]
then
	echo "Usage: $0 server emailDest"
	exit 1
fi

server=$1
database=weblogdb
#emailDest="carlos.azeglio@alacra.com,anthony.bruni@alacra.com"
emailDest=$2

#server=`get_xls_registry_value ${database} Server`
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`

filedir=$XLSDATA/pulse
filename=expiredDeals.txt
query="exec GetExpiredDeals"

perl $XLS/src/scripts/reports/pulse/ExpireDeals.pl ${server} ${database} ${logon} ${password} "$filedir" "$query" "$filename"
if [ $? != 0 ]
then
	echo "$0: Error from perl script, exiting"
	exit 1
fi

desc="Pulse Deal Expirations"

reportfile=${filedir}/${filename}


# Mail out the report

# convert all forward slashes to backslashes
fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
${reportfile}
HERE
)

#echo "blat - -body \"${desc}.  See attached\" -to ${emailDest} -subject \"${desc}\" -uuencode -attach ${fwslashpath1}" > test.sh
blat - -body "${desc}.  See attached" -to ${emailDest} -subject "${desc}" -attacht ${fwslashpath1}


exit 0
