sslogon=skarra
sspsw=skarra

# set the 'ignore prompts' flag (useful to remove for debugging)
iflag=-I-N


declare -i i
declare -i acct

# (removed 7000 for now)
accounts="6720 6751 6971 6973 6974 6975 6976 6977 6978 6979 6980 6981 6982 6983 6984 6985 6986 6987 6988 6989 6990 6991 6992 6993 6994 6995 6996 6997 6998 6999 7001 7002 7003 7004 7005 7006 7007 7008 7009 7011 7012 7013 7014"
#accounts="6720"
#i=0
#acct=6965
#while [ $i -lt 50 ]

for acct in $accounts
do
#	echo "$i, $acct"

	newdir=docs/alerts/${acct}
	if [ ! -d ${newdir} ]
	then

	# convert all forward slashes to backslashes
	fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
${newdir}
HERE
)

		ssdir="\$/xls/${newdir}"
		physdir="c:/usr/netscape/server/${newdir}"
		physdirdos="c:\\usr\\netscape\\server\\${fwslashpath1}"

		if [ ! -d ${physdir} ]
		then
			echo "Error, directory ${physdir} not found"
			exit 1
		fi

		echo "cd ${physdir}"
		cd ${physdir}

		# set the new project under alerts to be the current one
		echo "ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}"
		ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}

#echo "ABOUT TO CHECK OUT, PWD=${PWD}"
#read zz
		# check out the files
		echo "ss Checkout emailalert.xsl ${iflag} -GWR -GL${physdirdos} -Y${sslogon},${sspsw}"
		ss Checkout emailalert.xsl ${iflag} -GWR -GL${physdirdos} -Y${sslogon},${sspsw}
		echo "ss Checkout emailmobile.xsl ${iflag} -GWR -GL${physdirdos} -Y${sslogon},${sspsw}"
		ss Checkout emailmobile.xsl ${iflag} -GWR -GL${physdirdos} -Y${sslogon},${sspsw}

		echo "chmod +r+w ${physdir}/email*.xsl"
		chmod +r+w ${physdir}/email*.xsl
#echo "ABOUT TO COPY, PWD=${PWD}"
#read zz

		# copy the files from 7000
		echo "cp c:/usr/netscape/server/docs/alerts/7000/emailalert.xsl ${physdir}"
		cp c:/usr/netscape/server/docs/alerts/7000/emailalert.xsl ${physdir}
		echo "cp c:/usr/netscape/server/docs/alerts/7000/emailmobile.xsl ${physdir}"
		cp c:/usr/netscape/server/docs/alerts/7000/emailmobile.xsl ${physdir}
#echo "ABOUT TO CHECK IN"
#read zz
		# checkin the files
		echo "ss Checkin emailalert.xsl ${iflag} -Y${sslogon},${sspsw}"
		ss Checkin emailalert.xsl ${iflag} -Y${sslogon},${sspsw}
		echo "ss Checkin emailmobile.xsl ${iflag} -Y${sslogon},${sspsw}"
		ss Checkin emailmobile.xsl ${iflag} -Y${sslogon},${sspsw}
#echo "DONE WITH THIS ACCT"
#read zz

		if [ 1 = 0 ]
		then
			ssdir="\$/xls/docs/EmailCfg"
			physdir="c:/usr/netscape/server/docs/EmailCfg"
			physdirdos="c:\\usr\\netscape\\server\\EmailCfg"

			cd ${physdir}

			# set Emailcfg project to be the current one
			echo "ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}"
			ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}

			# check out the file
			echo "ss Checkout ${acct}.xml ${iflag} -Y${sslogon},${sspsw}"
			ss Checkout ${acct}.xml ${iflag} -Y${sslogon},${sspsw}

			# copy from 7000
			cp ${physdir}/7000.xml ${physdir}/${acct}.xml
			chmod +r+w ${physdir}/${acct}.xml

			# check in the file
			echo "ss Checkin ${acct}.xml ${iflag} -Y${sslogon},${sspsw}"
			ss Checkin ${acct}.xml ${iflag} -Y${sslogon},${sspsw}
		fi

	fi


#	let i=i+1
#	let acct=acct+1
done

exit 0
