# Routine to recursively split all *.pdf files

shname=splitallpdf.sh

# Parse the command line arguments
#	1 = Root Directory

if [ $# != 1 ]
then
	echo "Usage: ${shname} rootdir"
	exit 1
fi

# Save the runstring parameters in local variables
rcsroot=$1



# Make sure the top level tree exists in the rcs tree
if [ ! -d ${rcsroot} ]
then
	echo "${shname}: ${rcsroot} is not a directory"
	exit 1
fi


# Do for all the files in the directory
for filename in `ls ${rcsroot}`
do
	# See if this is a regular file - shouldn't happen
	if [ ! -d ${rcsroot}/${filename} ]
	then
		thisfile=${rcsroot}/${filename}
		thisext=${thisfile##*.}

		case $thisext in
		pdf)		doit=1;;
		PDF)		doit=1;;
		Pdf)		doit=1;;
		pDf)		doit=1;;
		pdF)		doit=1;;
		PDf)		doit=1;;
		pDF)		doit=1;;
		PdF)		doit=1;;
		*)			doit=0;;
		esac

		if [ "${doit}" = "1" ]
		then
#			echo "$XLS/src/scripts/utility/splitpdf.sh ${thisfile} ${rcsroot}"
			$XLS/src/scripts/utility/splitpdf.sh ${thisfile} ${rcsroot}
		fi

	else
		# file is a directory, so recurse into it
		${shname} ${rcsroot}/${filename}
		if [ $? != 0 ]
		then
			exit 1
		fi
	fi

done

exit 0
