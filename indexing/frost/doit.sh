isql /Uxls /Pxls /Sdata1 << HERE
truncate table xls..kwocw_frost
truncate table xls..kwocd_frost
truncate table xls..kwocx_frost
go
dump transaction xls with truncate_only
go
HERE

bcp xls..kwocw_frost in dictionary.dat /fdictionary.fmt /Uxls /Pxls /Sdata1 /b1000
bcp xls..kwocd_frost in documents.dat /fdocuments.fmt /Uxls /Pxls /Sdata1 /b1000
bcp xls..kwocx_frost in xref.dat /fxref.fmt /Uxls /Pxls /Sdata1 /b1000
