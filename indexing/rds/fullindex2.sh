database="rds"
TITLEBAR="${database} Index Update"

SCRIPTDIR=$XLS/src/scripts/indexing/${database}
LOGDIR=$XLSDATA/${database}
mkdir -p ${LOGDIR}

logfile=${LOGDIR}/fullindex2.log

${SCRIPTDIR}/rebuildindex2.sh > ${logfile} 2>&1
if [ $? != 0 ]
then
	blat ${logfile}  -t "administrators@xls.com" -s "${database} index Error"
	exit 1
fi
