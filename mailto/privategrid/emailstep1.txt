Welcome to the secure PrivateGrid membership process.

Please go to https://secure1.privategrid.com/pgrid/${_uuid,u}
where you can obtain membership in PrivateGrid.

Thank you.


Sincerely,
PrivateGrid Security Team


n.b. -- This letter was sent in response to a request originating from IP address ${_ip} by a user ${email}.
If you did not request this email please disregard the instructions, above, and forward this email to
unrequest@PrivateGrid.com so that we may work to block future attempts of the user at ${_ip} from bothering you
in the future.
