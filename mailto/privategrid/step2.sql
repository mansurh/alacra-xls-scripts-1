DECLARE @icount int

SELECT @icount = count(*) from pgrid_application where ipaddress = '${ipaddress,s}' or companydomain = '${companydomain,s}'

IF @icount = 0
BEGIN

	INSERT INTO pgrid_application 
	(id,companyname,companydomain,address1,address2,city,state,zip,country,contactname,contactphone,contactemail,contactcallback,ipaddress,ipisp,ipemailserver,ipdnsserver,billingmethod,userlogin,userpassword,submit_time)
	VALUES
	('${_uuid,s}','${companyname,s}','${companydomain,s}','${address1,s}','${address2,s}','${city,s}','${state,s}','${zip,s}','${country,s}','${contactname,s}','${contactphone,s}','${contactemail,s}','${contactcallback,s}','${ipaddress,s}','${ipisp,s}','${ipemailserver,s}','${ipdnsserver,s}','${billingmethod,s}','${userlogin,s}','${userpassword,s}',GETDATE())

END

ELSE

BEGIN

	PRINT '***DUPLICATE***'

END
GO
