class colors:
    GREEN  = '\x1b[32;1m'
    RED    = '\x1b[31;1m'
    YELLOW = '\x1b[33;1m'
    PURPLE = '\x1b[35;1m'
    WHITE  = '\x1b[37;1m'
    BLUEBG = '\x1b[44;1m'       # regular blue too dark in default windows console
    # CYAN   = '\x1b[36;1m' For some reason, sometimes doesn't work in mintty
    END    = '\x1b[0m'
